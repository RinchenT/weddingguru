<?php 
$activePage = basename($_SERVER['PHP_SELF'], ".php");
?>

<body>
<div class="navigation-menu">
  <header role="banner">
      <div class="container">
          <img src="img/small-logo.png">
      </div>
  </header>
  <nav class="navbar navbar-expand-md navbar-dark">
    <div class="container">
      <a class="navbar-brand" href="#">Wedding Guru</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mainNavBar" aria-controls="navbarsExample04" aria-expanded="false" aria-label="Toggle navigation">
        <i class="fa fa-bars"></i>
        </button>

        <div class="collapse navbar-collapse" id="mainNavBar">
          <ul class="navbar-nav mr-auto navbar-left">
            <li class="nav-item">
              <a class="nav-link <?php if ($activePage=="index") {echo "active"; } else  {echo "noactive";}?>" href="index.php">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link <?php if ($activePage=="venues") {echo "active"; } else  {echo "noactive";}?>" href="venues.php"> Venues</a>
            </li>
		 	<li class="nav-item">
              <a class="nav-link <?php if ($activePage=="about") {echo "active"; } else  {echo "noactive";}?>" href="about.php"> About</a>
            </li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li class="nav-item dropdown">
	        <?php if(isset($_SESSION['email'])): ?>
	            <a class="nav-link active dropdown-toggle" href="#" id="logoutDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		         <?php echo $_SESSION['first_name'] . " " . $_SESSION['last_name']; ?>
		        </a>
		        <div class="dropdown-menu float-right" aria-labelledby="logoutDropdown">
		        	<a class="dropdown-item" href="my-bookings.php">My Bookings</a>
		        	<a class="dropdown-item" href="api/logout.php">Logout</a>
		        </div>
            <?php else: ?>
	            <a class="nav-link <?php if ($activePage=="login") {echo "active"; } else  {echo "noactive";}?>" href="login.php"><i class="fa fa-sign-in"></i> Login</a>
            <?php endif; ?>
            </li>
          </ul>
        </div>
      </div> <!--.container-->
    </nav>
</div> <!--.navigation-menu-->
</body>