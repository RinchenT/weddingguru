<?php
require_once('api/venues.php');
if (session_status() == PHP_SESSION_NONE) {
	session_start();
}
?>

<html>
<head>
    <title>WeddingGuru: Venues</title>
    <?php include 'includes.php'; ?>
</head>

<body>
<?php include 'navbar.php'; ?>

<div class="main venues-page">
	<div class="container">
		<div class="row">
				<div class="col-md-3 venues-filter">
					<form id="filter-form" method="GET">
						<h5>REFINE</h5>
						<hr/>
						<h5>Location</h5>
						<hr/>
						<div class="form-check">
							<input class="form-check-input" type="checkbox" value="'Merseyside'" name="city" id="merseyside" checked>
							<label class="form-check-label" for="merseyside">Merseyside</label><br>
							<input class="form-check-input" type="checkbox" value="'North Wales'" name="city" id="north-wales" checked>
							<label class="form-check-label" for="north-wales">North Wales</label><br>
							<input class="form-check-input" type="checkbox" value="'Cheshire'" name="city" id="cheshire" checked>
							<label class="form-check-label" for="cheshire">Cheshire</label><br>
						</div><hr/>
						
						<h5>Min Capacity</h5>
						<hr/>
						<input type="range" name="capacity" id="filter_capacity" value="1" min="1" max="500" oninput="filter_capacity_display.value = filter_capacity.value">
						<output id="filter_capacity_display"></output>

						<h5>License Required?</h5>
						<hr/>
						<div class="form-check">
							<input class="form-check-input" type="radio" value="Yes" name="licensed" id="licensed-yes">
							<label class="form-check-label" for="licensed-yes">Yes</label><br>
							<input class="form-check-input" type="radio" value="No" name="licensed" id="licensed-no" checked>
							<label class="form-check-label" for="licensed-no">No</label><br>
						</div>
						<hr/>
						
						<h5>Gazebo Required?</h5>
						<hr/>
						<div class="form-check">
							<input class="form-check-input" type="radio" value="Yes" name="gazebo" id="gazebo-yes">
							<label class="form-check-label" for="gazebo-yes">Yes</label><br>
							<input class="form-check-input" type="radio" value="No" name="gazebo" id="gazebo-no" checked>
							<label class="form-check-label" for="gazebo-no">No</label><br>
						</div><br>
						<h5>Budget</h5>
						<hr/>
						<input type="range" name="price" id="filter_price" value="550" min="1" max="550" oninput="filter_price_display.value = filter_price.value">
						<output id="filter_price_display"></output>
						<hr/>
						<input type="button" id="filterBtn" class="btn btn-primary" value="Filter">
					</form>
				</div>
				<div class="col-md-9 venues-list" id="filtered-list">
				<?php 
				$listOfVenues = $db->getAllVenues();
				if($listOfVenues->num_rows > 0):
						
				while($row = $listOfVenues->fetch_assoc()) {
				?>
					<div class="row venue">
						<div class="col-md-5">
							<img src="img/venues/<?php echo $row['image'] . "/" . $row['image'] . '.jpeg';?>" alt="picture of venue">
						</div>
						<div class="col-md-7">
							<h6>Venue Name: </h6> <?php echo $row['name']; ?>
							<h6>Location:</h6>
							<p> <?php echo $row['city']; ?> </p>
							<h6>Capacity: </h6><?php echo $row['capacity'];?> <br>
							<h6>Price per day:</h6>&pound;<?php echo $row['price']; ?><br>
							<a href="venue.php?<?php echo $row['image']; ?>"><button type="button" class="btn btn-primary float-right">View Venue</button></a>
						</div>
					</div>
					<?php } endif; ?>
				</div>
			</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	$("#filterBtn").click(function() {      
		var queryString = $('#filter-form').serialize().split("&");
		  $.ajax({
		    type: "GET",
		    url: "api/filter.php",
		    data: {queryString},
		    dataType: "html",   
		     success: function(response){       
		    	 $("#filtered-list").replaceWith(response);         
	    	}      
		});
	});
});
</script>
    
</body>

</html>