<?php
require_once('api/include/functions.php');
if (session_status() == PHP_SESSION_NONE) {
	session_start();
}

$email = $_SESSION["email"];
$db = new Functions();
$bookings = $db->getUserBookings($email);

?>

<html>
<head>
    <title>WeddingGuru: My Bookings</title>
    <?php include 'includes.php'; ?>
</head>

<body>
<?php include 'navbar.php'; ?>

<div class="main my-bookings">
<div class="col-md-12">
	<div class="container">
	<h2>Bookings</h2>
	<?php 
		if($bookings->num_rows > 0):
		?>
		<?php 						
		while($row = $bookings->fetch_assoc()) {
		?>
		<div class="row bookings">
			<div class="jumbotron">
				<p><strong> Venue Name: </strong> <?php echo $row["venueName"]; ?> </p>
				<p><strong> Date: </strong> <?php echo $row["date"]; ?> </p>
				<p><strong> Price: </strong> &pound;<?php echo $row["price"]; ?>
				<p><strong> Reference ID: </strong> <?php echo $row["reference"]; ?> </p>	
			</div>
		</div>
	<?php } 
	else: ?>
	<h5>You have no bookings!</h5>
	<?php endif;
	?>
	
	</div>
	</div>
</div>
</body>
</html>