<?php 
if (session_status() == PHP_SESSION_NONE) {
	session_start();
}
?>

<html>
<head>
    <title>WeddingGuru: About</title>
    <?php include 'includes.php'; ?>
</head>

<body>
<?php include 'navbar.php'; ?>
<div class="main my-bookings">
	<div class="container">
		<h1> About </h1>
		<p> 
		This website was developed using HTML, CSS, JavaScript, PHP, Ajax and jQuery.
		</p>
		<p>All images used are non-copyright and have been taken from www.pexels.com and www.unsplash.com</p>
	</div>
</div>
</body>

</html>
