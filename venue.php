<?php
require_once ('api/venues.php');
if (session_status () == PHP_SESSION_NONE) {
	session_start ();
}

$query = $_SERVER ['QUERY_STRING'];
$venue = $db->getVenue ( $query );

$bedroom = $db->getBedrooms ( $venue ['name'] );
$catering = $db->getCatering ( $venue ['name'] );

$venue_total_price = $venue ['price'];

$single = null;
$double = null;
$premium = null;

while ( $row = $bedroom->fetch_assoc () ) {
	switch ($row ["size"]) {
		case "Single" :
			$single = $row;
			break;
		case "Double" :
			$double = $row;
			break;
		case "Premium" :
			$premium = $row;
			break;
	}
}
?>

<html>
<head>
<title>WeddingGuru: <?php echo $venue['name']; ?> </title>
    <?php include 'includes.php'; ?>
</head>

<body>
<?php include 'navbar.php'; ?>

<div class="main venue-page">
		<div class="container">
			<h2 class="text-center"> <?php echo $venue['name']; ?></h2>
			<br>
			<div id="venueCarousel" class="carousel slide" data-ride="carousel">
				<ol class="carousel-indicators">
					<li data-target="#venueCarousel" data-slide-to="0" class="active"></li>
					<li data-target="#venueCarousel" data-slide-to="1"></li>
				</ol>
				<div class="carousel-inner">
					<?php
					$folder = "img/venues/" . $venue ['image'] . "/*";
					
					$counter = 0;
					foreach ( glob ( $folder ) as $file ) {
						if ($counter == 0) {
							echo '
								<div class="carousel-item active">
									<img class="d-block w-100" src=' . $file . '>
								</div>';
						} else {
							echo '
								<div class="carousel-item">
									<img class="d-block w-100" src=' . $file . '>
								</div>';
						}
						$counter ++;
					}
					?>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-6">
					<h5>Venue Information</h5>
					<hr />
					<p> <?php echo $venue['description']; ?></p>
					<p>
						<strong>Price for one day</strong>: &pound;<?php echo $venue['price']; ?></p>
					<p>
						<strong>Capacity</strong>: <?php echo $venue['capacity']; ?></p>
					<p>
						<strong>Licensed?</strong>: <?php
						
						if ($venue ['licensed'] == 1) {
							echo "Yes";
						} else {
							echo "No";
						}
						?>
					</p>
					<?php
					if ($venue ['gazebo'] == 1) {
						echo "<p><strong>Gazebo Price</strong>: &pound;" . $venue ['gazeboPrice'] . "</p>";
					}
					?>
					<p>
						<strong>Parking Capacity</strong>: <?php echo $venue['parkingCapacity']; ?></p>
					<p>
						<strong>Address</strong>:
					</p>
					<p>
					<?php
					echo $venue ['addressLine1'];
					echo "<br>" . $venue ['addressLine2'];
					echo $venue ['city'] . "<br>";
					echo $venue ['postcode'] . "<br>";
					?>
					</p>
					<hr />
					<br>
				</div>
				<div class="col-md-6">
					<h5>Bedroom Information</h5>
					<hr />
					<p>
						<em>If you would like to book rooms with the venue, please enter
							the amount of rooms for each bedroom size;</em>
					</p>
					<p>Single - <?php echo $single['number_of_rooms'] . " rooms available"?></p>
					<p>Weekday Price: &pound;<?php echo $single['weekday_price'] ?> </p>
					<p>Weekend Price: &pound;<?php echo $single['weekend_price'] ?> </p>

					<hr />
					<p>Double - <?php echo $double['number_of_rooms'] . " rooms available"?></p>
					<p>Weekday Price: &pound;<?php echo $double['weekday_price'] ?> </p>
					<p>Weekend Price: &pound;<?php echo $double['weekend_price'] ?> </p>

					<hr />
					<p>Premium - <?php echo $premium['number_of_rooms'] . " rooms available"?></p>
					<p>Weekday Price: &pound;<?php echo $premium['weekday_price'] ?> </p>
					<p>Weekend Price: &pound;<?php echo $premium['weekend_price'] ?> </p>

					<hr />
				</div>

				<div class="col-md-12">
					<h5>Catering Information</h5>
					<hr />
					<p>
						<em>The catering option we offer is shown as below;</em>
					</p>
					
					<?php while($row = $catering->fetch_assoc()) {?>
					<?php
						$cateringID = $row ['id'];
						$cateringPrice = $row ['price'];
						?>
					<p>Type: <?php echo $row['type'] ?></p>
					<p>Rating(1-5): <?php echo $row['rating'] ?></p>
					<p>Price: &pound;<?php echo $row['price'] ?> per head</p>
					<p>Description: <?php echo $row['description'] ?></p>
					<hr />
					
					<?php }?>
				</div>
			</div>

		<?php if(isset($_SESSION['email'])): ?>
			<form action="api/venues.php" method="POST" class="booking-form">

				<h5>Booking Details</h5>
				<hr />
		
				<?php if(isset($_SESSION["booking"]['price'])): ?>
				<p>You have booked the venue for <?php echo $_SESSION['booking']['date']; ?>
				
				<p>Your total comes to: &pound;<?php echo $_SESSION["booking"]["price"]; ?> </p>
				<p>Confirm booking below:</p>
				<input type="Submit" value="Confirm Booking" name="confirmBtn"
					class="btn btn-success"> <input type="Submit"
					value="Cancel Booking" name="cancelBtn" class="btn btn-danger">
				<?php else: ?>		
								<div class="row">
					<div class="col-md-4">
						<p>Single Bedroom (<?php echo "MAX " . $single['number_of_rooms'] ?>):</p>
						<input type="number" name="room-single" class="form-control"
							value=0 min=0 max=<?php echo $single['number_of_rooms']?>> <br>
					</div>
					<div class="col-md-4">
						<p>Double Bedroom (<?php echo "MAX " . $double['number_of_rooms'] ?>):</p>
						<input type="number" name="room-double" class="form-control"
							value=0 min=0 max=<?php echo $double['number_of_rooms']?>> <br>
					</div>
					<div class="col-md-4">
						<p>Premium Bedroom (<?php echo "MAX " . $premium['number_of_rooms'] ?>):</p>
						<input type="number" name="room-premium" class="form-control"
							value=0 min=0 max=<?php echo $premium['number_of_rooms']?>> <br>

					</div>
				</div>
				
				<?php if($venue['gazebo']): ?>
				<div class="row">
					<div class="col-md-4">
						<p>Would you like to book a Gazebo?</p>
						<input type="radio" id="gazeboChoice1" name="gazebo" value="yes">
						<label for="gazeboChoice1">Yes</label> <input type="radio"
							id="gazeboChoice2" name="gazebo" value="no" checked> . <label
							for="gazeboChoice2">No</label>
					</div>
				</div>
				<?php endif; ?>
				<div class="row">
					<div class="col-md-12">
						<p>If you would like to book the catering menu above, please enter
							the amount of people you would like to book for:</p>
						<input type="number" class="form-control" name="cateringAmount"
							value=0 min=0 max=<?php echo $venue['capacity']; ?>> <input
							type="hidden" name="cateringPrice"
							value="<?php echo $cateringPrice ?>"> <input type="hidden"
							name="cateringID" value="<?php echo $cateringID ?>">
					</div>
				</div>

				<?php
					foreach ( $single as $value ) {
						echo '<input type="hidden" name="single[]" value="' . $value . '">';
					}
					
					foreach ( $double as $value ) {
						echo '<input type="hidden" name="double[]" value="' . $value . '">';
					}
					foreach ( $premium as $value ) {
						echo '<input type="hidden" name="premium[]" value="' . $value . '">';
					}
					?>
					<br> <label for="date">Enter booking date:</label> <input
					value=<?php echo $venue['name']; ?> name="venue"
					style="display: none"> 
					<input type="date" class="form-control" name="date" id="date" min="<?php echo date("Y-m-d");?>" required> <br> <input type="Submit"
					value="Review Booking" name="reviewBtn" class="btn btn-success">
				<?php endif; ?>

				<?php
				if (isset ( $_SESSION ["bookingConfirmation"] )) {
					echo "<h4>" . $_SESSION ["bookingConfirmation"] . "</h4>";
					$_SESSION ["bookingConfirmation"] = null;
				}
				?>
			</form>
			<?php else: ?>
			<p>Please log in to create a booking</p>
			<?php endif; ?>
		</div>
	</div>
</body>
</html>