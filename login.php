<?php

?>

<html>
<head>
    <title>WeddingGuru: Home</title>
    <?php include 'includes.php'; ?>
</head>

<body>
<?php include 'navbar.php'; ?>

<div class="main">
	<div class="container jumbotron login-container">
		<div class="row">
			<div class="col-md-6">
			<h4>Enter your login details</h4>
				<form class="signin-form" action="api/login.php" method="POST">	
					<div class="col-md-12">
 						<div class="form-group">
					    	<label for="email">Email address</label>
							<input type="email" name="email" class="form-control" title="Enter your email address" required>
					  	</div>
					  	 <div class="form-group">
					    	<label for="password">Password</label>
							<input type="password" class="form-control" name="password" id="password"><br>
					  	</div>
						<input type="submit" class="btn btn-primary" value="Sign In"><br>
					</div>
					
					<?php if (isset($_SESSION['Error'])){
						echo $_SESSION['Error'];
					}?>
				</form>
			</div>
			<div class="col-md-6">
			<h4>Register</h4>
				<p> New to Wedding Guru? Register here!</p>	
				<a href="register.php"><button type="button" class="btn btn-primary">Register</button></a>
			</div>
		</div>	
	</div>
</div>
</body>

</html>