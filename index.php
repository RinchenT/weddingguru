<?php
if (session_status () == PHP_SESSION_NONE) {
	session_start ();
}
?>

<html>
<head>
<title>WeddingGuru: Home</title>
    <?php include 'includes.php'; ?>
</head>

<body>
<?php include 'navbar.php'; ?>
<div class="container">
		<div id="mainCarousel" class="carousel slide" data-ride="carousel">
			<ol class="carousel-indicators">
				<li data-target="#mainCarousel" data-slide-to="0" class="active"></li>
				<li data-target="#mainCarousel" data-slide-to="1"></li>
				<li data-target="#mainCarousel" data-slide-to="2"></li>
			</ol>
			<div class="carousel-inner">
				<div class="carousel-item active">
					<img class="d-block w-100" src="img/aisle.jpg" alt="First slide">
				</div>
				<div class="carousel-item">
					<img class="d-block w-100" src="img/couple.jpg" alt="Second slide">
				</div>
				<div class="carousel-item">
					<img class="d-block w-100" src="img/love-heart.jpg"
						alt="Third slide">
				</div>
			</div>
		</div>

		<div id="main">
			<br>
			<h1 class="text-center">Welcome</h1>
			<p>Welcome to Wedding Guru. We bring the highest quality of work and
				offer a wide range of venues through the north west of the UK</p>
			<p>Our venues are located in the following region:</p>
			<ul>
				<li>Merseyside</li>
				<li>Cheshire</li>
				<li>North Wales</li>
			</ul>

		</div>

	</div>

</body>
</html>