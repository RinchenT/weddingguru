<?php 
	include 'include/functions.php';
	
	$db = new Functions();
	
	$filterInfo = $_GET["queryString"];
	$mysqlquery = "SELECT * FROM venues WHERE ";
	
	$amountOfIterations = 0;
	
	echo '<div class="col-md-9 venues-list" id="filtered-list">';
	$amountOfIterations = sizeof($filterInfo) - 4;
	if(sizeof($filterInfo) >4){
		$mysqlquery = $mysqlquery . "(";
		for($i = 0; $i < $amountOfIterations; $i++){
			if($i == ($amountOfIterations-1)){
				$mysqlquery = $mysqlquery . str_replace("%20", " ", $filterInfo[$i] . ") AND ");
			} else {
				$mysqlquery = $mysqlquery . str_replace("%20", " ", $filterInfo[$i] . " OR ");
			}
		}
	} 
		
	$filterInfo[$amountOfIterations] = str_replace("capacity=", "capacity>=", $filterInfo[$amountOfIterations]); 
	$filterInfo[$amountOfIterations+1] = str_replace("Yes", 1 ,$filterInfo[$amountOfIterations+1]);
	$filterInfo[$amountOfIterations+2] = str_replace("Yes", 1 ,$filterInfo[$amountOfIterations+2]);
		
	$mysqlquery = $mysqlquery . $filterInfo[$amountOfIterations];
	
	if(strpos($filterInfo[$amountOfIterations+1], "1") > 0){
		$mysqlquery = $mysqlquery . " AND " . $filterInfo[$amountOfIterations+1];
	}
	
	if(strpos($filterInfo[$amountOfIterations+2], "1") > 0){
		$mysqlquery = $mysqlquery . " AND " . $filterInfo[$amountOfIterations+2];
	}

	$filterInfo[$amountOfIterations+3] = str_replace("price=", "price<=", $filterInfo[$amountOfIterations+3]);
	$mysqlquery = $mysqlquery . " AND " . $filterInfo[$amountOfIterations+3];
	
	$venues = $db->getVenueByFilter($mysqlquery);
	
	if($venues->num_rows == 0){
		echo "<h4> We have no venues for your filter options!</h4>";
	}
	while($row = $venues->fetch_assoc()) {?>
			<div class="row venue">
				<div class="col-md-5">
					<img src="img/venues/<?php echo $row['image'] . "/" . $row['image'] . '.jpeg';?>" alt="picture of venue">
				</div>
	<div class="col-md-7">
		<h6>Venue Name: </h6> <?php echo $row['name']; ?>
		<h6>Location:</h6>
		<p> <?php echo $row['city']; ?> </p>
		<h6>Capacity: </h6><?php echo $row['capacity'];?> <br>
		<h6>Price per day:</h6>&pound;<?php echo $row['price']; ?><br>
		<a href="venue.php?<?php echo $row['image']; ?>"><button type="button" class="btn btn-primary float-right">View Venue</button></a>
	</div>
</div>
					<?php }
					
	echo '</div>';
	
?>