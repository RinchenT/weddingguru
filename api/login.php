<?php 
session_start();
require_once 'include/functions.php';

$db = new Functions();

if(isset($_POST['email']) && isset($_POST['password'])){
	$email = $_POST['email'];
	$password = $_POST['password'];
	
	$user = $db->authenticate($email, $password);	
	
	if ($user != false) {
		echo $user["email"];
		$_SESSION['loggedin'] = true;
		$_SESSION["email"] = $user["email"];
		$_SESSION["first_name"] = $user["first_name"];
		$_SESSION["last_name"] = $user["last_name"];
		header("Location: ../index.php");
	} else {
		$_SESSION['Error'] = "Your email / password is Incorrect. Please try again!";
		header("Location:../login.php");
	}
}
?>