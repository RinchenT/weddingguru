<?php
session_start();
require_once 'include/functions.php';
$db = new Functions();

if (isset($_POST['first_name']) && isset($_POST['last_name']) && isset($_POST['email'])  && isset($_POST['password']) && isset($_POST['confirm_password'])) {
	if($_POST['password'] == $_POST['confirm_password']){
		$first_name = $_POST['first_name'];
		$last_name = $_POST['last_name'];
		$email = $_POST['email'];
		$password = $_POST['password'];
		
		$user = $db->createUser($email, $first_name, $last_name, $password);
		
		if($user){
			header("Location:../login.php");
		} else {
			header("Location: ../register.php");
			$_SESSION['Error'] = "<h4>Error: This email has already been used to register - please sign in!</h4>";
		}
	}
} else {
	echo "ERROR";
}
?>