<?php
class Functions {
	
	private $conn;
	
	// constructor
	function __construct() {
		require'conn.php';
		// connecting to database
		$db = new Connect();
		$this->conn = $db->connect();
	}
	
	// destructor
	function __destruct() {
		
	}
	
	/**
	 * Creating a new user
	 */
	public function createUser($email, $first_name, $last_name, $password) {
		$encrypted_password = password_hash($password, PASSWORD_DEFAULT);
		
		if($this->findUser($email)){
			header('../register.php');
			return false;
		} else {
			$query = $this->conn->prepare("INSERT INTO users(email, first_name, last_name, encrypted_password) VALUES(?, ?, ?, ?)");
			$query->bind_param("ssss", $email, $first_name, $last_name, $encrypted_password);
			$result = $query->execute();
			
			$query->close();
			
			if ($result) {
				$query = $this->conn->prepare("SELECT * FROM users WHERE email = ?");
				$query->bind_param("s", $email);
				$query->execute();
				$user = $query->get_result()->fetch_assoc();
				$query->close();
				
				return $user;
			} else {
				return false;
			}
		}	
	}
	
	/**
	 * Login 
	 * 
	 */
	
	public function authenticate($email, $password){
		$query = $this->conn->prepare("SELECT * FROM users WHERE email = ?");
		$query->bind_param('s', $email);
		$query->execute();
		
		$user = $query->get_result()->fetch_assoc();
		
		$query->close();
		
		if(password_verify($password, $user["encrypted_password"])) {
			return $user;
		} else {
			return null;
		}
	}
	
	public function findUser($email){
		
		$query = $this->conn->prepare("SELECT email from users WHERE email = ?");
		$query->bind_param("s", $email);
		$query->execute();
		$query->store_result();
		
		if ($query->num_rows > 0) {
			// user existed
			$query->close();
			return true;
		} else {
			// user not existed
			$query->close();
			return false;
		}
	}
	
	public function getAllVenues(){
		$result = $this->conn->query("SELECT * FROM venues");
		
		return $result;
	}
	
	public function getVenue($venueName){
		$query = $this->conn->prepare("SELECT * FROM venues WHERE image = ?");
		$query->bind_param("s", $venueName);
		$query->execute();
		$query = $query->get_result()->fetch_assoc();
		
		if($query == NULL){
			$query = $this->conn->prepare("SELECT * FROM venues WHERE name = ?");
			$query->bind_param("s", $venueName);
			$query->execute();
			$query = $query->get_result()->fetch_assoc();
		}
		return $query;
	}
	
	public function createBooking($date, $venue, $price, $gazebo, $cateringID){
		$query = $this->conn->prepare("SELECT * FROM bookings WHERE date = ? AND venueName = ?");
		$query->bind_param("ss", $date, $venue);
		$query->execute();
		$query = $query->get_result()->fetch_assoc();
		
		$bookingConfirmed = false;
		
		var_dump($cateringID);
		if($query == NULL){
			$bookingReference = substr(uniqid(rand()), 0, 5);
			$email = $_SESSION['email'];
			
			$query = $this->conn->prepare("INSERT INTO bookings(reference, userEmail, venueName, date, price, gazebo, cateringID) VALUES(?, ?, ?, ?, ?, ?, ?)");
			$query->bind_param("sssssis", $bookingReference, $email, $venue, $date, $price, $gazebo, $cateringID);
			$query->execute();
			$query->close();
						
			$bookingConfirmed = true;
		}
		return $bookingConfirmed;
	}

	public function getUserBookings($email){
		$query = $this->conn->prepare("SELECT * FROM bookings WHERE userEmail = ?");
		$query->bind_param("s", $email);
		$query->execute();
		$query = $query->get_result();
		return $query;
	}
	
	public function getBedrooms($venue){
		$query = $this->conn->prepare("SELECT * FROM bedroom WHERE venueName = ?");
		$query->bind_param("s", $venue);
		$query->execute();
		$query = $query->get_result();
		
		return $query;
	}
	
	public function getCatering($venue){
		$query = $this->conn->prepare("SELECT * FROM catering WHERE venueName = ?");
		$query->bind_param("s", $venue);
		$query->execute();
		$query = $query->get_result();
		
		return $query;
	}
	
	public function getVenueByFilter($filterQuery){
		$query = $this->conn->query($filterQuery);
		return $query;
	}
	
}
?>