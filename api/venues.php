<?php
if (session_status () == PHP_SESSION_NONE) {
	session_start ();
}
require_once ('include/functions.php');

$db = new Functions ();

if ($_SERVER ['REQUEST_METHOD'] === 'POST') {
	if (isset ( $_POST ['reviewBtn'] )) {
		// Post variables
		$date = $_POST ['date'];
		
		$singleRoom = $_POST['single'];
		$singleRoomAmount = $_POST['room-single'];
		
		$premiumRoom = $_POST['premium'];
		$premiumRoomAmount = $_POST['room-premium'];
		
		$doubleRoom = $_POST['double'];
		$doubleRoomAmount = $_POST['room-double'];
		
		$cateringPrice = $_POST['cateringPrice'];
		$cateringAmount = $_POST['cateringAmount'];
		$cateringID = $_POST['cateringID'];
		
		$gazebo = $_POST['gazebo'];	
		$venue = $db->getVenue($singleRoom[0]);
		$price = $venue['price'];
		
		// Checking to see if user has requested for a room
		if($premiumRoomAmount > 0|| $doubleRoomAmount > 0 || $singleRoomAmount > 0){
			if(isWeekend($date)){
				$price += ($premiumRoomAmount * $premiumRoom[3]);
				$price += ($singleRoomAmount * $singleRoom[3]);
				$price += ($doubleRoomAmount * $doubleRoom[3]);
			} else {
				$price += ($premiumRoomAmount * $premiumRoom[2]);
				$price += ($singleRoomAmount * $singleRoom[2]);
				$price += ($doubleRoomAmount * $doubleRoom[2]);
			}
		}
		
		// Gazebo check
		if($gazebo == "yes"){
			$price += $venue['gazeboPrice'];
			$_SESSION['booking']['gazebo'] = 1;
		} else {
			$_SESSION['booking']['gazebo'] = 0;
		}
		
		// Catering check
		if($cateringAmount > 0){
			$price += ($cateringAmount * $cateringPrice);
		}
		
		// Setting the booking information
		$catering['price'] = $cateringPrice;
		$catering['amount'] = $cateringAmount;
		$catering['ID'] = $cateringID;
		
		$_SESSION['booking']['date'] = $date;
		$_SESSION['booking']['price'] = $price;
		$_SESSION['booking']['venue'] = $venue;
		$_SESSION['booking']['catering'] = $catering;
		
	} else if(isset($_POST['confirmBtn'])){
		// Booking details
		$date = $_SESSION['booking']['date'];
		$venue = $_SESSION['booking']['venue'];
		$price = $_SESSION['booking']['price'];
		$gazebo =$_SESSION['booking']['gazebo'];
		
		$cateringID = null;
		
		// Setting the ID of the catering if requested
		if($_SESSION['booking']['catering']['amount'] > 0){
			$cateringID = $_SESSION['booking']['catering']['ID'];
		}
		$result = $db->createBooking ( $date, $venue['name'], $price, $gazebo, $cateringID);
		
		// If the venue has been booked for that day...
		if ($result == false) {
			$_SESSION ["bookingConfirmation"] = "Unfortunately, this venue has been booked for your specified date!";
		} else {
			$_SESSION ["bookingConfirmation"] = "You have successfully booked this venue!";
		}
		unset($_SESSION['booking']);
	} else if(isset($_POST['cancelBtn'])){
		unset($_SESSION['booking']);
	}
	
	header ( 'Location: ' . $_SERVER ['HTTP_REFERER'] );
}

function isWeekend($date) {
	return (date('N', strtotime($date)) >= 6);
}
?>