<?php
if (session_status () == PHP_SESSION_NONE) {
	session_start ();
}
?>

<html>
<head>
<title>WeddingGuru: Home</title>
    <?php include 'includes.php'; ?>
</head>

<body>
<?php include 'navbar.php'; ?>

<div class="main">
		<div class="container jumbotron">
			<form class="signin-form" action="api/register.php" method="POST">
				<div class="form-group">
					<label for="email">Email address</label> <input type="email"
						name="email" class="form-control" title="Enter your email address"
						required>
				</div>
				<div class="form-group">
					<label for="first_name">First Name</label> <input type="text"
						name="first_name" class="form-control"
						title="Enter your first name" required>
				</div>
				<div class="form-group">
					<label for="last_name">Last Name</label> <input type="text"
						name="last_name" class="form-control" title="Enter your surname"
						required>
				</div>
				<div class="form-group">
					<label for="password">Password</label> <input type="password"
						name="password" class="form-control"
						title="Please enter a password" required>
				</div>
				<div class="form-group">
					<label for="confirm_password">Confirm Password</label> <input
						type="password" name="confirm_password" class="form-control"
						title="Please Re-enter your password" required>
				</div>
				<div id="form-errors">
			<?php if(isset($_SESSION['Error'])):?>
				<p class="error"><?php
				echo $_SESSION ['Error'];
				unset ( $_SESSION ['Error'] );
				session_destroy ();
				?></p>
				<?php endif;?>
			</div>

				<input type="submit" class="btn btn-default" value="Sign Up"><br> <label>Already
					have an account? <a class="signupBtn" href="login.php">Sign In</a>
				</label>
			</form>
		</div>
	</div>
</body>

</html>